import React, { Suspense } from "react";
import "./App.css";
import Layout from "./components/Layout/Layout";
import { connect } from "react-redux";
import * as AuthAction from "./store/auth/actions";
import { Route, BrowserRouter, Switch, Redirect } from "react-router-dom";
import Radium, { StyleRoot } from "radium";
import Loading from "./components/UI/Loading/Loading";
import Axios from "axios";
//
const Landing = React.lazy(() => import("./containers/Landing/landing"));
const Profile = React.lazy(() => import("./containers/Profile/profile"));
const App = (props) => {
  let auth = false;
  const token = localStorage.getItem("token");
  if (token) {
    auth = true;
    Axios.defaults.headers.common["token"] = token;
    props.autoLogin(token);
  }
  let routes = (
    <Switch>
      {/* index */}
      <Route
        exact
        path={"/"}
        render={() => (
          <Suspense fallback={<Loading />}>
            <Landing />
          </Suspense>
        )}
      />
      <Redirect to="/" />
    </Switch>
  );
  // if Authentication
  if (auth) {
    routes = (
      <Switch>
        {/* <Route
          exact
          path={"/"}
          render={() => (
            <Suspense fallback={<Loading />}>
              <Landing />
            </Suspense>
          )}
        /> */}
        <Route
          exact
          path={"/profile"}
          render={() => (
            <Suspense fallback={<Loading />}>
              <Profile />
            </Suspense>
          )}
        />
        <Redirect to="/profile" />
      </Switch>
    );
  }

  return (
    <StyleRoot>
      <div className="App">
        <BrowserRouter>
          <Layout>{routes}</Layout>
        </BrowserRouter>
      </div>
    </StyleRoot>
  );
};

const setStore = (dispatch) => {
  return {
    autoLogin: (res) => dispatch(AuthAction.autoLogin(res)),
  };
};

const getStore = (state) => {
  return {
    is_auth: state.auth.is_auth,
  };
};

export default connect(getStore, setStore)(Radium(App));
