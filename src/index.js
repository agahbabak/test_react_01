import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./App.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import Axios from "axios";
import store from "./store";
import { Provider } from "react-redux";
// import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import {
  createMuiTheme,
  ThemeProvider,
  StylesProvider,
  jssPreset,
} from "@material-ui/core/styles";
import rtl from "jss-rtl";
import { create } from "jss";
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
const theme = createMuiTheme({
  typography: {
    fontFamily: ["Handlee"].join(","),
  },
  direction: "rtl",
  palette: {
    secondary: {
      main: "#000",
    },
    primary: {
      main: "#fc0384",
    },
    link: {
      main: "blue",
    },
  },
});
// Config Axios
Axios.defaults.baseURL = "https://reqres.in/api";
Axios.defaults.headers.post["Content-Type"] = "application/json";

ReactDOM.render(
  <React.Fragment>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <StylesProvider jss={jss}>
          <App />
        </StylesProvider>
      </Provider>
    </ThemeProvider>
  </React.Fragment>,
  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
