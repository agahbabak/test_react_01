import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import * as commonActions from "../../store/common/actions";
import { connect } from "react-redux";
import "./landing.css";
import Register from "../../components/Register/Register";
class Landing extends Component {
  componentDidMount() {}
  render() {
    return (
      <div className={"landing"}>
        <div className={"content"}>
          <Register />
        </div>
      </div>
    );
  }
}

const setStore = (dispatch) => ({
  toggleLoading: (res) => dispatch(commonActions.toggleLoading(res)),
});
const getStore = (state) => ({
  is_auth: state.auth.is_auth,
});

export default withRouter(connect(getStore, setStore)(Landing));
