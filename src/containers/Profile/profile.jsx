import React, { Component } from "react";
import Axios from "axios";
import User from "../../components/Card/User/User";
import Dialog from "../../components/UI/Dialog/Dialog";
class Profile extends Component {
  state = {
    users: [],
    dialog: false,
    currentUser: {},
  };
  componentDidMount() {
    Axios.get("/users?page=1")
      .then((res) => {
        this.setState({ users: [...res.data.data] });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  getUser = (user) => {
    // Axios.get(`/users/${user.id}`)
    //   .then((res) => {
    //     this.setState({ currentUser: res.data.data, dialog: true });
    //   })
    //   .catch((err) => console.log(err));
    this.setState({ currentUser: user, dialog: true });
  };
  render() {
    return (
      <div style={{ padding: "100px 10px" }}>
        <div>this is profile</div>
        {this.state.users.length &&
          this.state.users.map((user) => (
            <User
              showDetails={() => this.getUser(user)}
              key={user.id}
              user={user}
            />
          ))}
        <Dialog
          style={{ backgroundColor: "#000" }}
          direction="up"
          fullScreen={false}
          open={this.state.dialog}
          onClose={() => this.setState({ dialog: false })}
        >
          <div style={{ width: 350, maxWidth: "96%" }}>
            <User user={this.state.currentUser} />
          </div>
          {/* <Suspense fallback={<Loading />}>
            <Register />
          </Suspense> */}
        </Dialog>
      </div>
    );
  }
}

export default Profile;
