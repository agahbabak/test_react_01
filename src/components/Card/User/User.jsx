import React from "react";
import Image from "../../UI/Image/Image";
import "./User.css";
export default (props) => {
  return (
    <div className={"userCard"} onClick={props.showDetails}>
      <div className={"text"}>
        <div>{props.user.email}</div>
        <div>{props.user.first_name}</div>
        <div>{props.user.last_name}</div>
      </div>
      <div className={"image"}>
        <Image
          style={{ borderRadius: 10 }}
          aspectRatio={1}
          src={props.user.avatar}
          preSrc=""
        />
      </div>
    </div>
  );
};
