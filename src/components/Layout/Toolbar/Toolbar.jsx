import React from "react";
import { NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import * as commonAction from "../../../store/common/actions";
import * as authActions from "../../../store/auth/actions";
const Toolbar = (props) => {
  const styles = makeStyles((theme) => ({
    root: {
      position: "fixed",
      top: 0,
      width: "100%",
      zIndex: 100,
      transition: ".3s",
      backgroundColor: "#FFF",
      boxShadow: "0 5px 5px 0 rgba(0, 0, 0, 0.116)",
      "& ul": {
        margin: 0,
        padding: "0 20px",
        height: 75,
        alignItems: "center",
        listStyle: "none",
        display: "flex",
        justifyContent: "space-between",
      },
      "& ul li": {
        cursor: "pointer",
        color: "#000",
        padding: "0 10px",
      },
    },
    logo: {
      color: theme.palette.primary.main,
      fontWeight: "bold",
      fontSize: 25,
    },
  }));
  const classes = styles();
  return (
    <React.Fragment>
      <nav className={classes.root}>
        <ul>
          <li>
            <NavLink exact to={"/"}>
              <span className={classes.logo}>Test</span>
            </NavLink>
          </li>
          <ul className={classes.right}>
            {props.is_auth && (
              <React.Fragment>
                <li onClick={props.logout}>
                  <span className={"icon-297"}>logout</span>
                </li>
              </React.Fragment>
            )}
          </ul>
        </ul>
      </nav>
    </React.Fragment>
  );
};

const setStore = (dispatch) => {
  return {
    toggleRegisterDialog: (res) =>
      dispatch(commonAction.toggleRegisterDialog(res)),
    logout: (res) => dispatch(authActions.logout(res)),
  };
};

const getStore = (state) => {
  return {
    is_auth: state.auth.is_auth,
    token: state.auth.token,
    width: state.common.size.width,
    scrollPosition: state.common.scrollPosition,
    OffsetToChangeToolbarTheme: state.common.OffsetToChangeToolbarTheme,
  };
};

export default connect(getStore, setStore)(withRouter(Toolbar));
