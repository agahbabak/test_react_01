import React, { Component } from "react";
import { withRouter } from "react-router";
import Toolbar from "./Toolbar/Toolbar";
import { connect } from "react-redux";
import * as commonAction from "../../store/common/actions";
import Dialog from "../UI/Dialog/Dialog";
import Loading from "../UI/Loading/Loading";

class Layout extends Component {
  constructor(props) {
    super(props);

    this.props.setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }
  state = {};

  componentDidMount() {
    window.addEventListener("scroll", this.scroll_listener);
    window.addEventListener("resize", this.resize_listener);
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scroll_listener);
    window.removeEventListener("resize", this.resize_listener);
  }

  scroll_listener = (e) =>
    this.props.setScrollPosition(e.srcElement.body.scrollTop);
  resize_listener = (e) =>
    this.props.setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });

  render() {
    return (
      <section>
        <Toolbar />
        <main>{this.props.children}</main>
        <Dialog
          style={{ backgroundColor: "#000" }}
          direction="up"
          fullScreen={false}
          scroll="body"
          backgroundColor={"#000"}
          open={this.props.registerDialog}
        >
          {/* <Suspense fallback={<Loading />}>
            <Register />
          </Suspense> */}
        </Dialog>
        <Dialog fullScreen={true} open={this.props.loading}>
          <Loading position="fixed" />
        </Dialog>
      </section>
    );
  }
}

const setStore = (dispatch) => {
  return {
    setScrollPosition: (res) => dispatch(commonAction.setScrollPosition(res)),
    setWindowSize: (res) => dispatch(commonAction.setWindowSize(res)),
    toggleRegisterDialog: (res) =>
      dispatch(commonAction.toggleRegisterDialog(res)),
  };
};

const getStore = (state) => {
  return {
    registerDialog: state.common.registerDialog,
    modalContent: state.common.modalContent,
    loading: state.common.loading,
    is_auth: state.auth.is_auth,
  };
};

export default connect(getStore, setStore)(withRouter(Layout));
