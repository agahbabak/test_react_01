import React, { useState } from "react";
import "./Register.css";
import { connect } from "react-redux";
import * as commonAction from "../../store/common/actions";
import * as authAction from "../../store/auth/actions";
import Btn from "../UI/Buttons/Btn/Btn";
import InputM1 from "../UI/Inputs/InputM1";
import Axios from "axios";
import { useForm } from "react-hook-form";
const SingIn = (props) => {
  const [loading, setloading] = useState();
  const [error, setError] = useState({ public: "" });
  const { register, handleSubmit, errors } = useForm();
  const handleLogin = (form) => {
    setloading(true);
    Axios.post("login", form)
      .then((res) => {
        setloading(false);
        props.login({ token: res.data.token, user: { username: "user" } });
        // props.toggleRegisterDialog(false);
      })
      .catch((err) => {
        setloading(false);
        console.log(err.response.data.error);
        setError({ public: err.response.data.error, ...error });
      });
  };
  return (
    <form onSubmit={handleSubmit(handleLogin)}>
      <div className={"input"}>
        <InputM1
          label={"email"}
          name={"email"}
          type={"email"}
          defaultValue={"eve.holt@reqres.in"}
          error={errors.email ? true : false}
          helperText={errors.email && "email is not valid"}
          inputRef={register({ required: true, minLength: 8 })}
          fullWidth={true}
        />
      </div>
      <div className={"input"}></div>
      <InputM1
        label={"password"}
        name={"password"}
        type={"password"}
        defaultValue={"pistol"}
        error={errors.email ? true : false}
        helperText={errors.email && "password is not valid"}
        inputRef={register({ required: true, minLength: 6 })}
        fullWidth={true}
      />
      <div className={"error"}>{error.public}</div>
      <div className={"button"}>
        <Btn type="submit" loading={loading} className="primary" size="large">
          sing in
        </Btn>
      </div>
      <div onClick={props.setStep} className={"link_text"}>
        Don't have an account yet? <span className={"link"}>Register now</span>
      </div>
    </form>
  );
};

const setStore = (dispatch) => {
  return {
    toggleRegisterDialog: (res) =>
      dispatch(commonAction.toggleRegisterDialog(res)),
    login: (res) => dispatch(authAction.login(res)),
  };
};

export default connect(null, setStore)(SingIn);
