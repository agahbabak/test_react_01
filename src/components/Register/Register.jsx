import React, { useState, Suspense } from "react";
import Loading from "../UI/Loading/Loading";
import "./Register.css";
import { connect } from "react-redux";
import * as commonAction from "../../store/common/actions";
const SingIn = React.lazy(() => import("./SingIn"));
const SingUp = React.lazy(() => import("./SingUp"));
const Register = (props) => {
  const [step, setStep] = useState(1);

  let form = null;

  if (step === 1) {
    form = (
      <Suspense fallback={<Loading />}>
        <SingIn setStep={() => setStep(2)} />
      </Suspense>
    );
  } else {
    form = (
      <Suspense fallback={<Loading />}>
        <SingUp setStep={() => setStep(1)} />
      </Suspense>
    );
  }
  return (
    <div className={"register"}>
      <div className={"close"} onClick={props.toggleRegisterDialog}>
        close
      </div>
      <div style={{ padding: 15 }}>
        <h2 className={"title"}>{step ===1 ? "Sign in" : "sing up"}</h2>
        {form}
      </div>
    </div>
  );
};
const setStore = (dispatch) => {
  return {
    toggleRegisterDialog: (res) =>
      dispatch(commonAction.toggleRegisterDialog(res)),
  };
};

export default connect(null, setStore)(Register);
