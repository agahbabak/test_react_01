import React, { Component } from "react";
import { connect } from "react-redux";
import * as commonAction from "../../../store/common/actions";

class SetToolbarTheme extends Component {
  componentDidMount() {
    this.props.changeToolbarOffset(
      document.getElementById("changeToolbarTheme").offsetTop -
        this.props.offset
    );
  }

  render() {
    return <div id="changeToolbarTheme"></div>;
  }
}

const setStore = (dispatch) => ({
  changeToolbarOffset: (res) => dispatch(commonAction.changeToolbarOffset(res)),
});

export default connect(null, setStore)(SetToolbarTheme);
