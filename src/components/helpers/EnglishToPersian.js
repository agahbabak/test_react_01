const EnglishToPersian = (v) => {
  const e = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  const array = v.toString().split("");
  let newNumber = [];
  array.forEach((i) => {
    if (i === ".") {
      newNumber.push(".");
    } else
      e.forEach((j, index) => {
        if (i == index) {
          newNumber.push(e[index]);
          return;
        }
      });
  });
  return newNumber.join("");
};
export default EnglishToPersian;
