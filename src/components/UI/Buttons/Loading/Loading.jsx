import React from 'react'
import './Loading.css'

export default (props) => {
    const classes = props.className ? props.className : "";
    return (
        <div className={"lds-ring " + classes}><div></div><div></div><div></div><div></div></div>
    )
}