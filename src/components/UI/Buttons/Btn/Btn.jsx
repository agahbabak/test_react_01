import React from "react";
import Loading from "../Loading/Loading";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
const styles = makeStyles((theme) => ({
  root: {
    borderRadius: 5,
    width: "100%",
    textTransform: "capitalize",
    "&.MuiButton-sizeSmall": {
      height: 30,
    },
    "&.MuiButton-sizeLarge": {
      height: 52,
    },
    "&.primary.MuiButton-outlined": {
      color: theme.palette.primary.main,
      borderColor: theme.palette.primary.main,
    },
    "&.primary.MuiButton-contained": {
      color: "#FFF",
      backgroundColor: theme.palette.primary.main,
    },
  },
}));

const Btn = (props) => {
  const otherClasses = props.className ? " " + props.className : "";
  const classes = styles();
  return (
    <Button
      color="primary"
      className={classes.root + otherClasses}
      focusRipple
      variant={props.variant}
      disableElevation
      size={props.size}
      type={props.type}
      disabled={props.disabled}
      onClick={!props.loading ? props.onClick : null}
    >
      <span style={{ paddingTop: 2 }}>
        {props.loading ? <Loading /> : props.children}
      </span>
    </Button>
  );
};

Btn.defaultProps = {
  type: "button",
  variant: "contained",
};

export default Btn;
