import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";
import "./Dialog.css";
const MyDialog = (props) => {


  const cloneProps = { ...props };
  const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction={cloneProps.direction} ref={ref} {...props} />;
  });
  let dialog = (
    <Dialog
      className={"Dialog"}
      open={props.open}
      onClose={props.onClose}
      fullScreen={props.fullScreen}
      scroll={props.scroll}
    >
      <DialogContent>{props.children}</DialogContent>
    </Dialog>
  );
  if (props.direction) {
    dialog = (
      <Dialog
        className={"Dialog"}
        TransitionComponent={Transition}
        open={props.open}
        onClose={props.onClose}
        fullScreen={props.fullScreen}
        scroll={props.scroll}
      >
        <DialogContent>{props.children}</DialogContent>
      </Dialog>
    );
  }
  return <div>{props.open ? dialog : null}</div>;
};
export default MyDialog;
