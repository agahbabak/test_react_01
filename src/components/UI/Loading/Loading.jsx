import React from "react";

import "./Loading.css";
const Loading = (props) => {
  return (
    <React.Fragment>
      {props.value ? (
        <div
          style={{
            zIndex:"1000",
            position: props.position,
            top: 0,
            right: 0,
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            transition:"1s",
            // backgroundColor: "#FFF9",
            // backdropFilter: "blur(1px)"
          }}
        >
          <div className="lds-spinner">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      ) : null}
    </React.Fragment>
  );
};

Loading.defaultProps = {
  value: true,
  position:"absolute"
};
export default Loading;
