import { transform } from "lodash";
import React from "react";
import Image from "./Image";
const ImageRemove = (props) => {
  return (
    <div style={{ position: "relative" }}>
      <div
        style={{
          background: "#0009",
          color:"#fff",
          borderRadius: "100%",
          position: "absolute",
          right: -7,
          top: -7,
          zIndex: 2,
          width: 14,
          height: 14,
          display:"flex",
          justifyContent:"center",
          alignItems:"center",
          border:"3px solid #fff",
          transform:"rotate(45deg)"
        }}
      >
        +
      </div>
      <Image aspectRatio={1} src={props.src} borderRadius={10} />
    </div>
  );
};

ImageRemove.defaultProps = {
  preSrc: "http://localhost:8000/",
  className: "",
  borderRadius: 0,
};

export default ImageRemove;
