import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import LazyLoad from "react-lazyload";

const IMG = (props) => {
  const style = makeStyles(() => ({
    root: {
      overflow: "hidden",
      display: "flex",
      alignItems: "center",
      borderRadius: props.borderRadius,
    },
    img: {
      position: "relative",
      width: "100%",
      paddingTop: (1 / props.aspectRatio) * 100 + "%",
      backgroundImage: `url(${props.preSrc + props.src})`,
      backgroundSize: props.bgSize,
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
    },
  }));
  const classes = style();
  let content = (
    <React.Fragment>
      {props.aspectRatio ? (
        <div
          className={props.className + " " + classes.root}
          style={props.style}
          onClick={props.onClick ? () => props.onClick() : null}
        >
          <div className={classes.img}></div>
        </div>
      ) : (
        <img
          alt={props.alt}
          style={{
            userSelect: "none",
            MozWindowDragging: "false",
            ...props.styles,
          }}
          src={props.preSrc + props.src}
        />
      )}
    </React.Fragment>
  );
  if (props.lazy) {
    content = (
      <LazyLoad once>
        {props.aspectRatio ? (
          <div
            className={props.className + " " + classes.root}
            style={props.style}
            onClick={props.onClick ? () => props.onClick() : null}
          >
            <div className={classes.img}></div>
          </div>
        ) : (
          <img
            style={{
              userSelect: "none",
              MozWindowDragging: "false",
              ...props.styles,
            }}
            alt={props.alt}
            src={props.preSrc + props.src}
          />
        )}
      </LazyLoad>
    );
  }
  return content;
};

IMG.defaultProps = {
  preSrc: "http://localhost:8000/",
  className: "",
  borderRadius: 0,
  bgSize: "cover",
  alt:""
};

export default IMG;
