import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

export default (props) => {
  const rootStyles = makeStyles(() => ({
    root: {
      display: "flex",
      // width:"100%",
      "& .MuiFormLabel-root.Mui-focused": {
        color: "#828282",
      },
    },
  }));
  const useStylesReddit = makeStyles((theme) => ({
    root: {
      border: "1px solid #e2e2e1",
      overflow: "hidden",
      borderRadius: 5,
      backgroundColor: "#FFF",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
      "& .MuiFormControl-root ": {
        display: "flex",
      },
      "& .MuiInputBase-input": {
        // direction: props.type === "number" ? "ltr!important" : "rtl",
      },
      "&:hover": {
        backgroundColor: "#fff",
      },
      "&$focused": {
        backgroundColor: "#fff",
        // boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
        borderColor: theme.palette.primary.main,
      },
    },
    focused: {},
  }));
  const classes = useStylesReddit();
  const rootClasses = rootStyles();
  return (
    <div className={rootClasses.root}>
      <TextField
        fullWidth={props.fullWidth}
        InputProps={{ classes, disableUnderline: true }}
        className={classes.margin}
        {...props}
        variant="filled"
      />
    </div>
  );
};
