import React, { Component } from "react";
import "./TopImage.css";
import { connect } from "react-redux";
class TopImage extends Component {
  render() {
    return (
      <div
        className={"topImage " + this.props.className}
        style={{
          backgroundImage: "url(" + this.props.url + ")",
          transform:
            "translateY(" +
            (this.props.scrollPosition - this.props.scrollPosition / 1.5) +
            "px)",
        }}
      >
        
        <div
          style={{
            opacity: 1 - this.props.scrollPosition / 600,
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

const getStore = (state) => {
  return {
    scrollPosition: state.common.scrollPosition,
  };
};

export default connect(getStore, null)(TopImage);
