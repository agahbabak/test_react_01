import {
  TOGGLE_REGISTER_DIALOG,
  SET_SCROLL_POSITION,
  CHANGE_TOOLBAR_THEME,
  SET_WINDOW_SIZE,
  TOGGLE_LOADING,
} from "../../constants/actionTypes";

export const toggleRegisterDialog = (value) => {
  return {
    type: TOGGLE_REGISTER_DIALOG,
    value,
  };
};

export const setScrollPosition = (value) => {
  return {
    type: SET_SCROLL_POSITION,
    value,
  };
};

export const setWindowSize = (value) => {
  return {
    type: SET_WINDOW_SIZE,
    value,
  };
};

export const changeToolbarOffset = (value) => {
  return {
    type: CHANGE_TOOLBAR_THEME,
    value,
  };
};

export const toggleLoading = (value) => {
  return {
    type: TOGGLE_LOADING,
    value,
  };
};
