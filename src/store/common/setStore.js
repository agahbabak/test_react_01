import { updateObject } from "../utility";

export const toggleRegisterDialog = (state, value) => {
  return updateObject(state, {
    registerDialog: !state.registerDialog,
  });
};

export const setScrollPosition = (state, action) => {
  return updateObject(state, { scrollPosition: action.value });
};

export const changeToolbarTheme = (state, action) => {
  return updateObject(state, { OffsetToChangeToolbarTheme: action.value });
};

export const setWindowSize = (state, size) => {
  return updateObject(state, { size });
};

export const toggleLoading = (state, loading) => {
  return updateObject(state, { loading });
};
