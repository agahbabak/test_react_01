import {
  TOGGLE_REGISTER_DIALOG,
  SET_SCROLL_POSITION,
  CHANGE_TOOLBAR_THEME,
  SET_WINDOW_SIZE,
  TOGGLE_LOADING,
} from "../../constants/actionTypes";

import * as setStore from "./setStore";

const initalState = {
  registerDialog: false,
  productDetailsDialog: false,
  loading: false,
  modalContent: "",
  modalCart: false,
  scrollPosition: 0,
  OffsetToChangeToolbarTheme: null,
  stack_images: "",
  size: {},
};

export default (state = initalState, action) => {
  switch (action.type) {
    case TOGGLE_REGISTER_DIALOG:
      return setStore.toggleRegisterDialog(state, action.value);
    case TOGGLE_LOADING:
      return setStore.toggleLoading(state, action.value);
    case SET_SCROLL_POSITION:
      return setStore.setScrollPosition(state, action);
    case SET_WINDOW_SIZE:
      return setStore.setWindowSize(state, action.value);
    case CHANGE_TOOLBAR_THEME:
      return setStore.changeToolbarTheme(state, action);

    default:
      return state;
  }
};
