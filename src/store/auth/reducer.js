import { LOGIN, LOGOUT, UPDATE_USER } from "../../constants/actionTypes";
import * as setStore from "./setStore";

const initalState = {
  is_auth: false,
  user: {},
  token: "",
};

export default (state = initalState, action) => {
  switch (action.type) {
    case LOGIN:
      return setStore.login(state, action.data);
    case LOGOUT:
      return setStore.logout(state, action);
    case UPDATE_USER:
      return setStore.updateUser(state, action.user);
    default:
      return state;
  }
};
