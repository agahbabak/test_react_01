import axios from "axios";
import { updateObject } from "../utility";

export const login = (state, data) => {
  return updateObject(state, {
    is_auth: true,
    token: data.token,
    user: data.me,
  });
};

export const updateUser = (state, user) => {
  return updateObject(state, {
    user: user,
  });
};

export const logout = (state, action) => {
  localStorage.removeItem("token");
  axios.defaults.headers.common["key"] = "";
  return updateObject(state, {
    is_auth: false,
    token: "",
    user: {},
  });
};
