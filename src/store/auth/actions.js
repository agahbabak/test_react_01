import Axios from "axios";
import { LOGIN, LOGOUT, UPDATE_USER } from "../../constants/actionTypes";

export const login = (data) => {
  if (data && data.token) {
    localStorage.setItem("token", data.token);
    Axios.defaults.headers.common["token"] = data.token;
  }
  return {
    type: LOGIN,
    data,
  };
};
export const autoLogin = (token) => {
  return (dispatch) => {
    dispatch(login({ token, me: {} }));
    // Axios.get("/me")
    //   .then((res) => {
    //     let me = res.data;
    //     dispatch(login({ token, me }));
    //   })
    //   .catch((err) => {
    //     dispatch(logout());
    //   });
  };
};

export const updateUser = (user) => {
  return {
    type: UPDATE_USER,
    user: user.me,
  };
};

export const logout = () => {
  return {
    type: LOGOUT,
  };
};
