import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import common from "./common/reducer";
import auth from "./auth/reducer";
const rootStore = combineReducers({
  common,
  auth,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(rootStore, composeEnhancers(applyMiddleware(thunk)));

